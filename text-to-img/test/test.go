package main

import (
	"fmt"
	"gitee.com/archn/act-red-go/text-to-img/toimage"
)

func main() {
	// Example usage of renderTextToPNG function.
	text := "新ღღ你不认识吗？A中国🐂나사랑해요12abα🌸double 🇨🇳 🧧ৡღ ℒℴѵℯ依·訫ꦿ⸙ ঞ 不留遗憾 ོꦿ ࿐ ₅₂ₒ ꧁哥 ໊是ꕥ电ꦿ工꧂"

	/*fonts := []string{
		"../fonts/ebrima.ttf",    //相对路径（相对于工作目录）
		"/etc/fonts/ebrima.ttf",  //绝对路径
	}*/

	//1. 指定文件路径自动生成（在工作目录）
	outputPath := toimage.ExecPath("outimg", "output.png")
	toimage.CreateDir(outputPath, true)

	//2. 指定为文件对象指针自动写入
	//outputPath, _ := os.CreateTemp("", "tmp-text-*.png")
	//defer os.Remove(outputPath.Name())

	// TextToPngArgs 参数
	// 可选参数
	//	Size  默认12px
	//	Color 默认为黑色 color.Black
	//	Width 默认单行自适应
	// 	Height 默认单行自适应

	err := toimage.RenderTextToPNG(text, nil, outputPath, toimage.TextToPngArgs{Size: 30})
	if err != nil {
		panic(err)
	}
	fmt.Printf("当前保存路径%+v\n", outputPath)
}
