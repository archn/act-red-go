package toimage

import (
	"errors"
	"fmt"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

type TextToPngArgs struct {
	Color               color.Color
	Size, Width, Height int
}

var fontDefault = []string{
	"fonts/NotoSerifCJKSCBlack.ttf", //中日韩+基础
	"fonts/fonts/seguiemj.ttf",      //基本图标
	"fonts/sylfaen.ttf",             //特殊符号
	"fonts/himalaya.ttf",            //特殊符号
	"fonts/Nirmala.ttf",             //特殊符号
	"fonts/LeelawUI.ttf",            //特殊符号
	"fonts/javatext.ttf",            //特殊符号
	"fonts/msgothic.ttc",            //特殊符号
	"fonts/tahoma.ttf",              //特殊符号
	"fonts/ebrima.ttf",              //特殊符号
}

// RenderTextToPNG
//
//	text 字符串  fontPaths字体路径集合 outputPath图片输出路径或图片文件对象 args其它属性配置 字体大小12|指定宽度|指定高度
//																		宽高不指定将根据字体大小自适应
func RenderTextToPNG(text string, fontPaths []string, outputOrFile any, args ...TextToPngArgs) error {
	var (
		col  = color.Color(color.Black)
		file *os.File
	)

	switch v := outputOrFile.(type) {
	case string:
		f, err := os.Create(v)
		if err != nil {
			return err
		}
		file = f
		defer f.Close()
	case *os.File:
		file = v
	default:
		return errors.New("outputOrFile type is 'Path' OR '*os.File'")
	}

	fonts := initFonts(fontPaths)

	var (
		fontSize  = 12
		imgWidth  = 10000
		imgHeight = 500
		isAuto    = true
	)
	if len(args) > 0 {
		if args[0].Color != nil {
			col = args[0].Color
		}
		if args[0].Size > 0 {
			fontSize = args[0].Size
		}
		if args[0].Width > 0 {
			imgWidth = args[0].Width
			isAuto = false
			if args[0].Height == 0 {
				args[0].Height = fontSize
			}
		}
		if args[0].Height > 0 {
			imgHeight = args[0].Height
			isAuto = false
		}
	}

	img := image.NewRGBA(image.Rect(0, 0, imgWidth, imgHeight))
	draw.Draw(img, img.Bounds(), &image.Uniform{C: color.Transparent}, image.Point{}, draw.Src)

	// Set up the font drawer.
	drawer := &font.Drawer{
		Dst:  img,
		Src:  image.NewUniform(col),
		Face: nil, // We will set this later.
	}

	// Keep track of the current position for drawing.
	var (
		x = 1
		y = 0
	)

	setFontFace := func(char rune) {
		for _, face := range fonts {
			r := face.Index(char)
			if r > 0 {
				drawer.Face = truetype.NewFace(face, &truetype.Options{
					Size:    float64(fontSize),
					Hinting: font.HintingFull,
				})
				return
			}
		}
		drawer.Face = basicfont.Face7x13
	}

	// Render each character of the text.
	for _, char := range text {
		setFontFace(char)
		advance, ok := drawer.Face.GlyphAdvance(char)
		if !ok {
			continue
		}
		drawer.Dot = fixed.Point26_6{
			X: fixed.I(x),
			Y: fixed.I(y + fontSize),
		}
		drawer.DrawString(string(char))
		x += int(advance) >> 6 // Move the cursor to the right.
	}

	if isAuto {
		imgT, err := trimTransparentPNG(img)
		if err != nil {
			return err
		}
		return png.Encode(file, imgT)
	}
	return png.Encode(file, img)
}

func initFonts(fontsPath []string) (fonts []*truetype.Font) {
	dir := ModePath() + "/../"
	exePath := ExecPath()
	l := len(fontsPath) - 1
	list := append(fontsPath, fontDefault...)
	for i, v := range list {
		path := v
		if l < i {
			path = filepath.Join(dir, v)
		} else {
			if !filepath.IsAbs(v) {
				path = filepath.Join(exePath, v)
			}
		}
		f, err := mustLoadFont(path)
		if err == nil {
			fonts = append(fonts, f)
		}
	}
	return
}

func ModePath() string {
	_, filename, _, _ := runtime.Caller(0)
	return filepath.Dir(filename)
}

func ExecPath(fileName ...string) string {
	exePath, err := os.Executable()
	if err != nil {
		fmt.Println("Error getting executable path:", err)
		return ""
	}
	dir := filepath.Dir(exePath)

	if len(fileName) > 0 {
		return filepath.Join(dir, filepath.Join(fileName...))
	}
	// 提取可执行文件所在的目录
	return dir
}

func mustLoadFont(path string) (f *truetype.Font, err error) {
	fontData, _err := os.ReadFile(path)
	if _err != nil {
		err = _err
		return
	}
	f, err = truetype.Parse(fontData)
	return
}

// trimTransparentPNG 裁切PNG图像中的透明区域
func trimTransparentPNG(src image.Image) (image.Image, error) {
	bounds := src.Bounds()
	minX, minY := bounds.Min.X, bounds.Min.Y
	maxX, maxY := minX, minY

	for y := minY; y < bounds.Max.Y; y++ {
		for x := minX; x < bounds.Max.X; x++ {
			_, _, _, a := src.At(x, y).RGBA()
			if a > 0 { // 假设非透明像素的Alpha值大于0
				if x > maxX {
					maxX = x
				}
				if y > maxY {
					maxY = y
				}
			}
		}
	}
	if maxX == minX && maxY == minY {
		return image.NewNRGBA(image.Rect(0, 0, 0, 0)), nil
	}

	// 创建一个新的图像，并复制非透明部分
	newBounds := image.Rect(0, 0, maxX-minX+2, maxY-minY+2)
	dst := image.NewNRGBA(newBounds)
	draw.Draw(dst, dst.Bounds(), src, image.Point{}, draw.Src)
	return dst, nil
}

// ConvertRemoteImageToPNG 图片强转png
func ConvertRemoteImageToPNG(imageURL string, output *os.File) error {
	resp, err := http.Get(imageURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if !isPNG(imageURL) {
		img, _, err := image.Decode(resp.Body)
		if err != nil {
			return err
		}
		err = png.Encode(output, img)
		if err != nil {
			return err
		}
	} else {
		io.Copy(output, resp.Body)
	}
	return nil
}

func isPNG(filename string) bool {
	return strings.HasSuffix(strings.ToLower(filename), ".png")
}

// CreateDir 创建指定的目录，包括所有必要的父目录。
//
//	isFile 如果路径为文件将过滤掉最后一级的图片
func CreateDir(dirPath string, isFile ...bool) error {
	if len(isFile) > 0 && isFile[0] {
		dirPath = filepath.Dir(dirPath)
	}
	if _, err := os.Stat(dirPath); os.IsNotExist(err) {
		err = os.MkdirAll(dirPath, 0755) // 0755 是目录权限（rwxr-xr-x）
		if err != nil {
			return fmt.Errorf("failed to create directory %s: %w", dirPath, err)
		}
		fmt.Printf("Directory %s created successfully\n", dirPath)
	} else if err != nil {
		return fmt.Errorf("failed to stat directory %s: %w", dirPath, err)
	} else {
		fmt.Printf("Directory %s already exists\n", dirPath)
	}
	return nil
}
