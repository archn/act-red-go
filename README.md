# TextToImg

#### 介绍
文字转图片，中日韩等特殊字符支持，需要其它特殊字体生成请自行加入列表（先匹配上的文字优先渲染）

#### 编译测试

```shell
cd text-to-img\test
go build -o .\test.exe .
.\test.exe
```
执行后即可看到同目录已经生成对应的图片

#### 使用说明

1. 引入扩展
`go get gitee.com/archn/act-red-go`
2. 加载依赖
`go mod tidy`
3. 导入方法
`import "gitee.com/archn/act-red-go/text-to-img/toimage"`
4. 使用方法：
   * toimage.RenderTextToPNG(text string, fontPaths []string, outputOrFile any, args ...TextToPngArgs) error
      > 文字转换为图片方法，用于常规海报生成等
   
     `fontPaths` 默认使用内置字体 如果要新增请注意 排序靠前的优先匹配
   
     `outputOrFile` 可以是文件名称路径（绝对路径请已/开头）、也可以是文件句柄os.File用于内容写入

     `args` 为可选参数，用于配置文字的颜色大小图片大小等基本信息详情请关注对应结构体，（默认12px|黑色|自适应）

   * toimage.ConvertRemoteImageToPNG(imageURL string, output *os.File) error 
      > 任意远程图片转成本地png
   * toimage.ExecPath([file])
      > 获取工作路径或生成工作路径下的文件路径 

      `file` 可选参数，携带文件名称创建路径

   * toimage.CreateDir(path,[isFile])
      > 创建路径，除了文件本身
   
      `path` 文件或目录路径
   
      `isFile` 可选参数，path是否为文件，true将过滤掉文件这一层去创建目录

   引入包以后直接使用对应方法即可